﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TileType { Grass, Hill, Mountain, Water, WaterDeep };

[System.Serializable]
public class HexTile
{
    public static float root3div2 = Mathf.Sqrt(3f) / 2f;                // 0.8660254038
    public static float twosubroot3div4 = (2 - Mathf.Sqrt(3f)) / 4f;    // 0.0669872981
    public static Vector3[] verteces = { new Vector3(0, 0, 1f), new Vector3(root3div2, 0, 0.5f), new Vector3(root3div2, 0, -0.5f), new Vector3(0, 0, -1f), new Vector3(-root3div2, 0, -0.5f), new Vector3(-root3div2, 0, 0.5f) };
    public static Vector2[] uv = { new Vector2(0.5f, 1f), new Vector2(1f - twosubroot3div4, 0.75f), new Vector2(1f - twosubroot3div4, 0.25f), new Vector2(0.5f, 0f), new Vector2(twosubroot3div4, 0.250f), new Vector2(twosubroot3div4, 0.75f) };

    public static int[] triangles = { 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0 };

    public int x, y;
    public TileType type;
    public Material material;
    public float high, low;
    public Vector3 center;
    public int movementCost;
    public bool supportStructure;
    public Entity entity;
    public Resource resource;

    public HexTile(int x, int y, Vector3 center, HexTile tile)
    {
        this.x = x;
        this.y = y;
        this.center = center;
        this.type = tile.type;
        this.movementCost = tile.movementCost;
        this.supportStructure = tile.supportStructure;
    }

    public static HexTile GetTileByCoord(int x, int y)
    {
        return Map.tiles[x, y];
    }

    public HexTile[] GetNeighbors()
    {
        List<HexTile> n = new List<HexTile>();

        if (x != 0)
            //Right
            n.Add(Map.tiles[x - 1, y]);

        if (x != Map.width - 1)
            //Left
            n.Add(Map.tiles[x + 1, y]);

        if (y != 0)
            //Down
            n.Add(Map.tiles[x, y - 1]);

        if (y != Map.height - 1)
            //Up
            n.Add(Map.tiles[x, y + 1]);

        if (y % 2 == 0)
        {
            if (x != 0 && y != Map.height - 1)
                n.Add(Map.tiles[x - 1, y + 1]);
            if (x != 0 && y != 0)
                n.Add(Map.tiles[x - 1, y - 1]);
        }
        else
        {
            if (x != Map.width - 1 && y != Map.height - 1)
                n.Add(Map.tiles[x + 1, y + 1]);
            if (x != Map.width - 1 && y != 0)
                n.Add(Map.tiles[x + 1, y - 1]);
        }
        return n.ToArray();
    }
    public HexTile[] GetTilesInRadius(int radius)
    {
        List<HexTile> n = new List<HexTile>();
        int xmin = 0, xmax = 0;
        for (int y = -radius; y <= radius; y++)
        {
            if (this.y + y < 0 || this.y + y >= Map.height)
                continue;

            if (this.y % 2 == 0)
            {
                xmin = -(radius - Mathf.FloorToInt(Mathf.Abs(y) / 2.0f));
                xmax = radius - Mathf.CeilToInt(Mathf.Abs(y) / 2.0f); ;
            }
            else
            {
                xmin = -(radius - Mathf.CeilToInt(Mathf.Abs(y) / 2.0f));
                xmax = radius - Mathf.FloorToInt(Mathf.Abs(y) / 2.0f);
            }

            for (int x = xmin; x <= xmax; x++)
            {
                if (this.x + x < 0 || this.x + x >= Map.width)
                    continue;

                n.Add(Map.tiles[this.x + x, this.y + y]);
            }
        }

        return n.ToArray();
    }

    public static Vector2 TileUV(TileType type)
    {
        switch(type)
        {
            case (TileType.Grass):
                return new Vector2(0, 3);
            case (TileType.Hill):
                return new Vector2(1, 3);
            case (TileType.Mountain):
                return new Vector2(2, 3);
            case (TileType.Water):
                return new Vector2(0, 2);
            case (TileType.WaterDeep):
                return new Vector2(1, 2);
        }

        return Vector3.zero;
    }        
}
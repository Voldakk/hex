﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using System.Collections;


public class ConstructionYard : Structure
{
    public int buildRadius;
    private Structure newStructure;
    private HexTile mouseoverTile;
    private HexTile[] validTiles;

    protected override void Start()
    {
        base.Start();
    }
    protected override void Update()
    {
        base.Update();

        if (newStructure == null)
            return;

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
        {
            GameObject.Destroy(newStructure.gameObject);
            newStructure = null;
            MouseManager.block = false;
            Map.ClearHighlights();
            return;
        }

        mouseoverTile = MouseManager.GetMouseOverTile();

        if (EventSystem.current.IsPointerOverGameObject() || mouseoverTile == null)
            return;

        newStructure.transform.position = mouseoverTile.center;

        if (Input.GetMouseButtonDown(0) && validTiles.Contains(mouseoverTile) && mouseoverTile.entity == null && mouseoverTile.supportStructure)
        {
            GameManager.CreateStructure(newStructure.type, team, mouseoverTile);
            GameObject.Destroy(newStructure.gameObject);
            newStructure = null;
            MouseManager.block = false;
            Map.ClearHighlights();
        }
    }

    public override void OnTurnEnd()
    {
        base.OnTurnEnd();
    }

    public override void OnSelect()
    {
        base.OnSelect();

        if (GameManager.team != team)
            return;

        UIManager.elements.constructionYardPanel.gameObject.SetActive(true);
        UIManager.elements.constructFactoryButton.onClick.RemoveAllListeners();
        UIManager.elements.constructFactoryButton.onClick.AddListener(() => ButtonClicked(StructureType.Factory));
        UIManager.elements.constructRefineryButton.onClick.RemoveAllListeners();
        UIManager.elements.constructRefineryButton.onClick.AddListener(() => ButtonClicked(StructureType.ConstructionYard));
        UIManager.elements.constructSiloButton.onClick.RemoveAllListeners();
        UIManager.elements.constructSiloButton.onClick.AddListener(() => ButtonClicked(StructureType.Silo));
    }

    public override void OnDeselect()
    {
        base.OnDeselect();
        MouseManager.block = false;
        UIManager.elements.constructionYardPanel.gameObject.SetActive(false);
        Map.ClearHighlights();
    }

    public void ButtonClicked(StructureType structureType)
    {
        MouseManager.block = true;
        Debug.Log(structureType);

        newStructure = GameManager.Instantiate(GameManager.prefabs.structures.First(s => s.GetComponent<Structure>().type == structureType)).GetComponent<Structure>();
        validTiles = tile.GetTilesInRadius(buildRadius);
        Map.HighlightTiles(validTiles, newStructure);
    }
}

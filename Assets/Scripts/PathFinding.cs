﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum PathNodeStatus { Untested, Open, Closed }
public class PathNode
{
    public int x, y, F, G, H;
    public PathNodeStatus status;
    public PathNode parent;
    public PathNode[] neighbours;

    public PathNode (int x, int y)
    {
        this.x = x;
        this.y = y;
        F = 0;
        G = 0;
        H = 0;
        status = PathNodeStatus.Untested;
    }
    public void GetNeighbours()
    {
        List<PathNode> n = new List<PathNode>();

        if (x != 0)
            //Right
            n.Add(PathFinding.nodes[x - 1, y]);

        if (x != Map.width - 1)
            //Left
            n.Add(PathFinding.nodes[x + 1, y]);

        if (y != 0)
            //Down
            n.Add(PathFinding.nodes[x, y - 1]);

        if (y != Map.height - 1)
            //Up
            n.Add(PathFinding.nodes[x, y + 1]);

        if(y%2 == 0)
        {
            if (x != 0 && y != Map.height - 1)
                n.Add(PathFinding.nodes[x-1, y + 1]);
            if (x != 0 && y != 0)
                n.Add(PathFinding.nodes[x-1, y - 1]);
        }
        else
        {
            if (x != Map.width - 1 && y != Map.height - 1)
                n.Add(PathFinding.nodes[x + 1, y + 1]);
            if (x != Map.width - 1 && y != 0)
                n.Add(PathFinding.nodes[x + 1, y - 1]);
        }
        neighbours = n.ToArray();
    }
}
public static class PathFinding
{
    public static PathNode[,] nodes;
    public static List<PathNode> openNodes;

    public static void Setup()
    {
        nodes = new PathNode[Map.width, Map.height];
        openNodes = new List<PathNode>();

        for (int x = 0; x < Map.width; x++)
        {
            for (int y = 0; y < Map.height; y++)
            {
                nodes[x, y] = new PathNode(x, y);
            }
        }
        for (int x = 0; x < Map.width; x++)
        {
            for (int y = 0; y < Map.height; y++)
            {
                nodes[x, y].GetNeighbours();
            }
        }
    }

    public static int MovementCost(PathNode from, PathNode to, Unit unit)
    {
        return MovementCost(null, Map.tiles[to.x, to.y], unit);
    }
    public static int MovementCost(HexTile from, HexTile to, Unit unit)
    {
        switch (unit.type)
        {
            case UnitType.SISUXA185:
                switch (to.type)
                {
                    case TileType.Grass:
                        return 1;
                    case TileType.Hill:
                        return 2;
                    case TileType.Mountain:
                    case TileType.Water:
                    case TileType.WaterDeep:
                        return -1;
                }
                break;
            case UnitType.Battleship:
                switch (to.type)
                {
                    case TileType.Grass:
                    case TileType.Hill: 
                    case TileType.Mountain:
                        return -1;
                    case TileType.Water:
                    case TileType.WaterDeep:
                        return 1;
                }
                break;
            case UnitType.Helicopter:
                switch (to.type)
                {
                    case TileType.Grass:
                    case TileType.Hill:
                    case TileType.Water:
                    case TileType.WaterDeep:
                        return 1;
                    case TileType.Mountain:
                        return 2;
                }
                break;
        }
        return -1;
    }

    public static bool CanMoveOn(PathNode tile, Unit unit)
    {
        return CanMoveOn(Map.tiles[tile.x, tile.y], unit);
    }
    public static bool CanMoveOn(HexTile tile, Unit unit, bool checkOccupied = true)
    {
        if (checkOccupied && tile.entity != null)
            return false;

        return MovementCost(null, tile, unit) >= 0;
    }
    public static bool CanAttack(HexTile tile, Unit unit)
    {
        return MovementCost(null, tile, unit) >= 0;
    }
    public static int HeuristicCost(PathNode from, PathNode to, Unit unit)
    {
        return Mathf.RoundToInt(Vector3.Distance(Map.tiles[to.x, to.y].center, Map.tiles[from.x, from.y].center)*0.5f);
    }
    public static PathNode GetLowF()
    {
        int low = int.MaxValue;
        int index = -1;
        int c = openNodes.Count;
        for (int i = 0; i < c; i++)
        {
            if (openNodes[i].F < low)
            {
                low = openNodes[i].F;
                index = i;
            }
        }
        return openNodes[index];
    }
    public static List<HexTile> GetPath(HexTile start, HexTile end, Unit unit)
    {
        //Clear nodes
        openNodes = new List<PathNode>();
        for (int x = 0; x < Map.width; x++)
        {
            for (int y = 0; y < Map.height; y++)
            {
                nodes[x, y].G = int.MaxValue;
                nodes[x, y].F = int.MaxValue;
                nodes[x, y].parent = null;
                nodes[x, y].status = PathNodeStatus.Untested;
            }
        }

        //Target node
        PathNode target = nodes[end.x, end.y];

        //Start node
        openNodes.Add(nodes[start.x, start.y]);
        openNodes[0].status = PathNodeStatus.Open;
        openNodes[0].G = 0;
        openNodes[0].H = HeuristicCost(openNodes[0], target, unit);
        openNodes[0].F = openNodes[0].H;

        PathNode current = openNodes[0];
        while (openNodes.Count > 0)
        {
            //current = openNodes.Aggregate((c, d) => c.F < d.F ? c : d);
            current = GetLowF();

            if (current == target)
                break;

            current.status = PathNodeStatus.Closed;
            openNodes.Remove(current);

            foreach (PathNode node in current.neighbours)
            {
                if (node.status == PathNodeStatus.Closed)
                    continue;

                if(!CanMoveOn(node, unit) && node != target)
                {
                    node.status = PathNodeStatus.Closed;
                    continue;
                }
                int newG = current.G + MovementCost(current, node, unit); ;
                if (node.status != PathNodeStatus.Open)
                {
                    node.status = PathNodeStatus.Open;
                    openNodes.Add(node);
                }
                else if (newG >= node.G)
                    continue;

                node.parent = current;
                node.G = newG;
                node.F = newG + HeuristicCost(node, target, unit);
            }
        }
        if (current != target)
            return new List<HexTile>();

        List<HexTile> path = new List<HexTile>();
        while (current != null)
        {
            path.Add(GetTile(current));
            current = current.parent;
        }
        path.Reverse();
        return path;
    }
    static HexTile GetTile(PathNode node)
    {
        return Map.tiles[node.x, node.y];
    }
}

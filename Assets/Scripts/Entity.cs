﻿using UnityEngine;
using System.Collections;

public abstract class Entity : MonoBehaviour
{
    protected bool isSelected = false;
    public int team;
    public string displayName;
    public int startHealth;
    public HexTile tile;
    public bool busy = false;
    [HideInInspector]
    public int health { get; private set; }
    protected bool inbetweenTurns = false;

    protected virtual void Start ()
    {
        GameManager.AddEntity(this);
        health = startHealth;
    }
    protected virtual void Update ()
    {
        if (inbetweenTurns)
            CompleteTurn();
    }

    public virtual void SetTile(HexTile newTile)
    {
        if (tile != null)
            tile.entity = null;

        tile = newTile;
        tile.entity = this;
        transform.position = tile.center;
    }

    public virtual void OnTurnEnd()
    {
        inbetweenTurns = true;
    }
    public virtual void OnTurnStart() { }

    public virtual void OnSelect() { isSelected = true; UpdateUI(); }
    public virtual void OnDeselect() { isSelected = false; }

    protected void CompleteTurn()
    {
        inbetweenTurns = false;
        GameManager.OnUnitTurnComplete();
    }

    public virtual bool Damage(int damage)
    {
        health -= damage;
        UpdateUI();

        if (health <= 0)
        {
            Death();
            return true;
        }
        return false;
    }
    public virtual void Death()
    {
        if (isSelected)
            OnDeselect();

        tile.entity = null;
        GameManager.RemoveEntity(this);
        Destroy(gameObject);
    }

    public virtual void UpdateUI() { }
}

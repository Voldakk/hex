﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class GameManager : MonoBehaviour
{
    [System.Serializable]
    public class Prefabs
    {
        public GameObject[] structures;
        public GameObject[] units;

        public GameObject tileHighlight;
        public Material tileHighlightGreen;
        public Material tileHighlightRed;
    }
    private static GameManager instance;
    public static Prefabs prefabs;
    public Prefabs _prefabs;

    public int teams;
    public static int team;
    public static int currentTeamTurn;

    public List<List<Entity>> entities;
    public int turnEndComplete;
    public int turnEndToComplete;
    public bool turnEnd = true;

    void Awake()
    {
        if (instance != null)
            Debug.LogError("More than one GameManager in scene!");
        else
            instance = this;

        prefabs = _prefabs;

        entities = new List<List<Entity>>();
        for (int i = 0; i < teams; i++)
        {
            entities.Add(new List<Entity>());
        }

        currentTeamTurn = teams - 1;
    }
    void Start()
    {
        CreateStructure(StructureType.ConstructionYard, 0, Map.tiles[43, 20]);
        CreateStructure(StructureType.ConstructionYard, 1, Map.tiles[40, 25]);
    }
    void Update()
    {
        UIManager.elements.endTurnButton.interactable = true;
        for (int i = 0; i < entities[currentTeamTurn].Count; i++)
        {
            if (entities[currentTeamTurn][i].busy)
                UIManager.elements.endTurnButton.interactable = false;
        }

        if (turnEnd)
        {
            if (turnEndComplete == turnEndToComplete)
            {
                currentTeamTurn++;
                if (currentTeamTurn == teams)
                    currentTeamTurn = 0;

                team = currentTeamTurn;
                StartTurn();
            }
        }
    }

    public static void AddEntity(Entity entity)
    {
        instance.entities[entity.team].Add(entity);
    }
    public static void RemoveEntity(Entity entity)
    {
        instance.entities[entity.team].Remove(entity);
    }

    public static void OnUnitTurnComplete()
    {
        instance.turnEndComplete++;
    }

    public void EndTurnButtonClicked()
    {
        EndTurn();
    }
    private void EndTurn()
    {
        UIManager.elements.endTurnButton.interactable = false;
        turnEnd = true;
        turnEndComplete = 0;
        turnEndToComplete = 0;

        foreach (Entity entity in entities[currentTeamTurn])
        {
            entity.OnTurnEnd();
            turnEndToComplete++;
        }
    }
    private void StartTurn()
    {
        UIManager.elements.endTurnButton.interactable = true;
        turnEnd = false;

        foreach (Entity entity in entities[currentTeamTurn])
        {
            entity.OnTurnStart();
        }
    }

    public static void CreateStructure(StructureType type, int team, HexTile tile)
    {
        Structure newStructure = GameObject.Instantiate(prefabs.structures.First(str => str.GetComponent<Structure>().type == type)).GetComponent<Structure>();
        newStructure.enabled = true;
        newStructure.team = team;
        newStructure.SetTile(tile);
    }
    public static void CreateUnit(UnitType type, int team, HexTile tile)
    {
        Unit newUnit = GameObject.Instantiate(prefabs.units.First(u => u.GetComponent<Unit>().type == type)).GetComponent<Unit>();
        newUnit.enabled = true;
        newUnit.team = team;
        newUnit.SetTile(tile);
    }
}
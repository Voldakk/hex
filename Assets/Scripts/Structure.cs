﻿using UnityEngine;
using System.Collections;

public enum StructureType { ConstructionYard, Factory, Refinery, Silo }
public class Structure : Entity
{
    public StructureType type;
    protected HexTile[] neighborTiles;

    protected override void Start()
    {
        base.Start();
        neighborTiles = tile.GetNeighbors();
    }
    public override void OnSelect()
    {
        base.OnSelect();
        UIManager.elements.selectedStructurePanel.gameObject.SetActive(true);
    }
    public override void OnDeselect()
    {
        base.OnDeselect();
        UIManager.elements.selectedStructurePanel.gameObject.SetActive(false);
    }
    public override void UpdateUI()
    {
        if (!isSelected)
            return;

        UIManager.elements.selectedStructureDisplayName.color = team == GameManager.team ? Color.black : Color.red;
        UIManager.elements.selectedStructureDisplayName.text = displayName;
        UIManager.elements.selectedStructureHealth.text = string.Format("Health: {0} / {1}", health, startHealth);
    }
}

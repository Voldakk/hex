﻿using UnityEngine;
using System.Collections.Generic;

public enum ResourceType { Forrest, Metal }

public class Resource
{
    public ResourceType type;
    public int amount;

    public Resource(ResourceType type, int amount)
    {
        this.type = type;
        this.amount = amount;
    }
    public Vector2 TileUV()
    {
        switch (type)
        {
            case (ResourceType.Forrest):
                return new Vector2(0, 3);
            case (ResourceType.Metal):
                return new Vector2(1, 3);
        }

        return Vector3.zero;
    }
}
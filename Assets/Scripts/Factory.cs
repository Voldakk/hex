﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using System.Collections;


public class Factory : Structure
{
    private Unit newUnit;
    private HexTile mouseoverTile;
    
    protected override void Update()
    {
        base.Update();

        if (newUnit == null)
            return;

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
        {
            GameObject.Destroy(newUnit.gameObject);
            newUnit = null;
            MouseManager.block = false;
            Map.ClearHighlights();
            return;
        }

        mouseoverTile = MouseManager.GetMouseOverTile();

        if (EventSystem.current.IsPointerOverGameObject() || mouseoverTile == null)
            return;

        newUnit.transform.position = mouseoverTile.center;

        if (Input.GetMouseButtonDown(0) && neighborTiles.Contains(mouseoverTile) && mouseoverTile.entity == null && PathFinding.CanMoveOn(mouseoverTile, newUnit))
        {
            GameManager.CreateUnit(newUnit.type, team, mouseoverTile);
            GameObject.Destroy(newUnit.gameObject);
            newUnit = null;
            MouseManager.block = false;
            Map.ClearHighlights();
        }
    }
    
    public override void OnTurnEnd()
    {
        base.OnTurnEnd();
    }

    public override void OnSelect()
    {
        base.OnSelect();

        if (GameManager.team != team)
            return;

        UIManager.elements.factoryPanel.gameObject.SetActive(true);
        UIManager.elements.buildSISUButton.onClick.RemoveAllListeners();
        UIManager.elements.buildSISUButton.onClick.AddListener(() => ButtonClicked(UnitType.SISUXA185));
    }

    public override void OnDeselect()
    {
        base.OnDeselect();
        MouseManager.block = false;
        UIManager.elements.factoryPanel.gameObject.SetActive(false);
        Map.ClearHighlights();
    }

    public void ButtonClicked(UnitType unitType)
    {
        MouseManager.block = true;
        Debug.Log(unitType);

        newUnit = GameManager.Instantiate(GameManager.prefabs.units.First(u => u.GetComponent<Unit>().type == unitType)).GetComponent<Unit>();
        Map.HighlightTiles(neighborTiles, newUnit);
    }
}

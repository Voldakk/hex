﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Map : MonoBehaviour
{
    public struct MeshData
    {
        public readonly Vector3[] verteces;
        public readonly List<int> triangles;
        public readonly Vector2[] uv;
        public readonly Vector2[] uv2;

        public MeshData(Vector3[] verteces, List<int> triangles, Vector2[] uv, Vector2[] uv2)
        {
            this.verteces = verteces;
            this.triangles = triangles;
            this.uv = uv;
            this.uv2 = uv2;
        }
    }
    [System.Serializable]
    public struct ResourceSpawnRate
    {
        public ResourceType type;
        public ResourceSpawnRateTile[] tiles;
    }
    [System.Serializable]
    public struct ResourceSpawnRateTile
    {
        public TileType tile;
        public float modifier;
    }

    //Tiles
    public List<HexTile> _tileTypes;
    public Dictionary<TileType, HexTile> tileTypes;

    //Map
    public int seed, resourceSeed;
    public static int width, height;
    public int _width, _height;
    public static HexTile[,] tiles;

    Vector2 offset = new Vector2(2 * HexTile.root3div2, 1.5f);
    public static Map instance;

    public ResourceSpawnRate[] resources;

    void Awake()
    {
        width = _width;
        height = _height;

        instance = this;

        tileTypes = new Dictionary<TileType, HexTile>();
        //Populate the dictonary
        foreach (HexTile tile in _tileTypes)
        {
            tileTypes.Add(tile.type, tile);
        }

        //Generate map
        GenerateMap();
    }

    private void GenerateMap()
    {
        tiles = new HexTile[width, height];

        float[,] noisemap = Noise.GenerateNoiseMap(width, height, seed, 15, 12, 0.5f, 2, Vector2.zero, Noise.NormalizeMode.Local);

        Random.InitState(resourceSeed);
        float value;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int i = 0; i < tileTypes.Count; i++)
                {
                    if (noisemap[x, y] < _tileTypes[i].high && noisemap[x, y] >= _tileTypes[i].low)
                        tiles[x, y] = new HexTile(x, y, new Vector3(offset.x * x + (y % 2 == 1 ? HexTile.root3div2 : 0), 0, offset.y * y), _tileTypes[i]);
                }
                
                for (int i = 0; i < resources.Length; i++)
                {
                    value = Random.value;

                    ResourceSpawnRateTile[] a = resources[i].tiles.Where(s => s.tile == tiles[x, y].type).ToArray();
                    if (a.Length > 0)
                        value *= a[0].modifier;
                    else
                        value = 0;

                    if (value > 0.5)
                        tiles[x, y].resource = new Resource(resources[i].type, Mathf.RoundToInt((value - 0.95f) * 1000));
                }
            }
        }
        PathFinding.Setup();
        GenerateMesh();
    }

    private void GenerateMesh()
    {
        Vector3[] verteces = new Vector3[width * height * 6];
        Vector2[] uv = new Vector2[width * height * 6];
        Vector2[] uv2 = new Vector2[width * height * 6];

        List<int> triangles = new List<int>();
        Vector2 tileUV;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                for (int i = 0; i < 6; i++)
                {
                    verteces[y * width * 6 + x * 6 + i] = HexTile.verteces[i] + new Vector3(offset.x * x + (y % 2 == 1 ? HexTile.root3div2 : 0), 0, offset.y * y);
                    tileUV = HexTile.uv[i] / 4;
                    tileUV.x += HexTile.TileUV(Map.tiles[x, y].type).x / 4;
                    tileUV.y += HexTile.TileUV(Map.tiles[x, y].type).y / 4;
                    uv[y * width * 6 + x * 6 + i] = tileUV;

                    if (Map.tiles[x, y].resource != null)
                    {
                        tileUV = HexTile.uv[i] / 4;
                        tileUV.x += Map.tiles[x, y].resource.TileUV().x / 4;
                        tileUV.y += Map.tiles[x, y].resource.TileUV().y / 4;
                        uv2[y * width * 6 + x * 6 + i] = tileUV;
                    }
                }
                for (int i = 0; i < 12; i++)
                {
                    triangles.Add(HexTile.triangles[i] + y * width * 6 + x * 6);
                }
            }
        }

        MeshData meshData = new MeshData(verteces, triangles, uv, uv2);
        CreateMesh(meshData);
    }

    private void CreateMesh(MeshData meshData)
    {
        Mesh mesh = new Mesh();

        mesh.vertices = meshData.verteces;

        mesh.SetTriangles(meshData.triangles, 0);

        mesh.uv = meshData.uv;
        mesh.uv2 = meshData.uv2;
        mesh.RecalculateNormals();

        GetComponent<MeshFilter>().mesh = mesh;

        GetComponent<MeshCollider>().sharedMesh = mesh;

        //GenerateResources();
    }

    private static List<Transform> highlights = new List<Transform>();
    public  static void HighlightTiles(HexTile[] tiles, Entity entity = null)
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            if(i >= highlights.Count)
                highlights.Add(GameObject.Instantiate(GameManager.prefabs.tileHighlight).transform);
            else
                highlights[i].gameObject.SetActive(true);

            highlights[i].position = tiles[i].center + Vector3.up * 0.1f;

            if (tiles[i].entity == null)
            {
                if (entity is Unit)
                {
                    if (entity == null || entity != null && PathFinding.CanMoveOn(tiles[i], (entity as Unit)))
                        highlights[i].GetComponent<MeshRenderer>().material = GameManager.prefabs.tileHighlightGreen;
                    else
                        highlights[i].GetComponent<MeshRenderer>().material = GameManager.prefabs.tileHighlightRed;
                }
                else if(entity is Structure)
                {
                    if(tiles[i].supportStructure)
                        highlights[i].GetComponent<MeshRenderer>().material = GameManager.prefabs.tileHighlightGreen;
                    else
                        highlights[i].GetComponent<MeshRenderer>().material = GameManager.prefabs.tileHighlightRed;
                }
            }
            else
            {
                highlights[i].GetComponent<MeshRenderer>().material = GameManager.prefabs.tileHighlightRed;
            }
        }
    }
    public static void ClearHighlights()
    {
        for (int i = 0; i < highlights.Count; i++)
        {
            highlights[i].gameObject.SetActive(false);
        }
    }

    public static HexTile GetTileByTriangleIndex(int triangle)
    {
        int x = Mathf.FloorToInt(triangle / (4 * Map.height));
        int y = Mathf.FloorToInt(triangle / 4) % Map.height;
        return Map.tiles[x, y];
    }
}
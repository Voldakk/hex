﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class MouseManager : MonoBehaviour
{
    Entity selectedEntity;
    public static bool block = false;

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject() || block)
            return;

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
        {
            TileClick(GetMouseOverTile(), Input.GetMouseButtonDown(0));
        }
    }
    public static HexTile GetMouseOverTile()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.collider.tag == "Map")
                return Map.GetTileByTriangleIndex(hit.triangleIndex);
        }
        return null;
    }
    void TileClick(HexTile tile, bool left)
    {
        if (tile == null)
            return;

        if (left)
        {
            Select(tile.entity);
        }
        if (selectedEntity == null || selectedEntity.team != GameManager.team)
            return;

        if (!left) // Right
        {
            if(selectedEntity is Unit)
            {
                if (tile.entity == null)
                {
                    if (PathFinding.CanMoveOn(tile, (selectedEntity as Unit)))
                    {
                        (selectedEntity as Unit).Move(tile);
                    }
                }
                else if (tile.entity.team != selectedEntity.team)
                {
                    if(PathFinding.CanAttack(tile, (selectedEntity as Unit)))
                    {
                        (selectedEntity as Unit).Attack(tile.entity);
                    }
                } 
            }
        }
    }
    private void Select(Entity entity)
    {
        if(selectedEntity != null)
            selectedEntity.OnDeselect();

        selectedEntity = entity;

        if (selectedEntity != null)
            selectedEntity.OnSelect();
    }
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour
{
    public static UIManager elements;

    public Button endTurnButton;

    public Transform selectedUnitPanel;
    [HideInInspector]
    public Text 
        selectedUnitDisplayName,
        selectedUnitHealth,
        selectedUnitCombatPower,
        selectedUnitActionPoints;

    public Transform selectedStructurePanel;
    [HideInInspector]
    public Text 
        selectedStructureDisplayName,
        selectedStructureHealth;

    public Transform constructionYardPanel;
    [HideInInspector]
    public Button 
        constructFactoryButton,
        constructRefineryButton,
        constructSiloButton;

    public Transform factoryPanel;
    [HideInInspector]
    public Button buildSISUButton;

    void Awake()
    {
        elements = this;

        selectedUnitDisplayName = selectedUnitPanel.Find("DisplayName").GetComponent<Text>();
        selectedUnitHealth = selectedUnitPanel.Find("Health").GetComponent<Text>();
        selectedUnitCombatPower = selectedUnitPanel.Find("CombatPower").GetComponent<Text>();
        selectedUnitActionPoints = selectedUnitPanel.Find("ActionPoints").GetComponent<Text>();

        selectedStructureDisplayName = selectedStructurePanel.Find("DisplayName").GetComponent<Text>();
        selectedStructureHealth = selectedStructurePanel.Find("Health").GetComponent<Text>();

        constructFactoryButton = constructionYardPanel.Find("ConstructFactoryButton").GetComponent<Button>();
        constructRefineryButton = constructionYardPanel.Find("ConstructRefineryButton").GetComponent<Button>();
        constructSiloButton = constructionYardPanel.Find("ConstructSiloButton").GetComponent<Button>();

        buildSISUButton = factoryPanel.Find("BuildSISUButton").GetComponent<Button>();
    }
}

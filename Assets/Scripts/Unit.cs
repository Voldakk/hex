﻿using UnityEngine;
using System.Collections.Generic;
using System;

public enum UnitType { Constructor, SISUXA185, Battleship, Helicopter }
public class Unit : Entity
{
    enum UnitOrder { None, Move, MoveAttack, Attack }

    public UnitType type;

    public bool canControl = true;
    public bool canAttack = true;

    public AudioSource audioMovement;
    public AudioSource audioAttack;

    public bool setStartPos = false;
    public int startX, startY;

    public float animationSpeed;
    public int actionPoints;
    public int combatPower;

    private List<HexTile> path;
    private UnitOrder orders;

    public int actionPointsLeft { get; private set; }
    public int accumulatedActionPoints { get; private set; }
    private int moving = 0;

    private Entity combatTarget;
    private HexTile targetTile;
    public float attackDuration;
    private float attackTimer = 0;

    protected override void Start()
    {
        base.Start();

        actionPointsLeft = 0;
        accumulatedActionPoints = 0;
        path = new List<HexTile>();

        if(setStartPos)
            SetTile(Map.tiles[startX, startY]);
    }
    protected override void Update()
    {
        //Draw path line (for debuging)
        if (path.Count > 1)
        {
            for (int i = 0; i < path.Count - 1; i++)
            {
                Debug.DrawLine(path[i].center + Vector3.up * 0.1f, path[i + 1].center + Vector3.up * 0.1f, Color.red);
            }
        }

        //Do our orders
        if(orders == UnitOrder.Move)
        {
            if (moving > 0)
            {
                AnimateMovement();
            }
            else if(inbetweenTurns)
            {
                actionPointsLeft = 0;
                CompleteTurn();
            }
        }
        else if(orders == UnitOrder.Attack)
        {
            if(moving > 0)
            {
                AnimateMovement();
            }
            else if(actionPointsLeft > 0 && path.Count == 2)
            {
                AnimateAttack();
            }
        }
        else if (orders == UnitOrder.MoveAttack)
        {
            if (moving > 0)
            {
                AnimateMovement();
            }
            else if (actionPointsLeft > 0 && path.Count == 2 && inbetweenTurns)
            {
                AnimateAttack();
            }
            else if (inbetweenTurns)
            {
                CompleteTurn();
            }
        }
        else if (inbetweenTurns)
        {
            CompleteTurn();
        }
    }

    private bool acceptInput()
    {
        return (!inbetweenTurns && !busy && canControl);
    }

    public override void OnTurnStart()
    {
        //Check if anything is blocking our current path
        if (orders == UnitOrder.Move || orders == UnitOrder.MoveAttack)
        {
            for (int i = 0; i < path.Count; i++)
            {
                if (!PathFinding.CanMoveOn(path[i], this))
                {
                    path = PathFinding.GetPath(tile, targetTile, this);
                    if (path.Count == 0)
                        orders = UnitOrder.None;

                    break;
                }
            }
            //If somehing is blocking out target when we're withing range
            if(orders != UnitOrder.MoveAttack && !PathFinding.CanMoveOn(targetTile, this) && path.Count <= actionPoints + 1)
            {
                orders = UnitOrder.None;
                path.Clear();
            }
        }

        //If moving we accumulate any remaining action points
        if (orders == UnitOrder.Move || orders == UnitOrder.MoveAttack)
            accumulatedActionPoints += actionPointsLeft;

        actionPointsLeft = actionPoints;

        //If out target has moved
        if (orders == UnitOrder.Attack || orders == UnitOrder.MoveAttack)
        {
            if (combatTarget.tile != targetTile)
            {
                orders = UnitOrder.None;
                path.Clear();
            }
        }

        //Update UI
        UpdateUI();
    }
    public override void OnTurnEnd()
    {
        base.OnTurnEnd();

        //If we have somewhere to go
        if (path.Count >= 2)
        {
            CalculateMovement();
        }
        else //Clear any remaining action points
        {
            actionPointsLeft = 0;
            accumulatedActionPoints = 0;
        }

        //Update UI
        UpdateUI();
    }

    private void CalculateMovement()
    {
        //Calculate number of tiles we can move this turn
        int pathIndex = 1;
        moving = 0;
        while (actionPointsLeft > 0 && pathIndex < (orders == UnitOrder.Move ? path.Count: path.Count-1))
        {
            int cost = PathFinding.MovementCost(path[pathIndex-1], path[pathIndex], this);
            if (cost > actionPointsLeft + accumulatedActionPoints)
            {
                accumulatedActionPoints += actionPointsLeft;
                actionPointsLeft = 0;
            }
            else
            {
                actionPointsLeft += accumulatedActionPoints;
                accumulatedActionPoints = 0;
                actionPointsLeft -= cost;

                moving++;
            }
            pathIndex++;
        }

        //Play sound if we should move
        if (moving > 0)
            audioMovement.Play();
    }

    private void AnimateMovement()
    {
        //Move smoothly between the tiles
        busy = true;
        Vector3 dir = path[1].center - transform.position;
        transform.Translate(Vector3.ClampMagnitude(dir.normalized * animationSpeed * Time.deltaTime, dir.magnitude), Space.World);
        transform.rotation = Quaternion.LookRotation(dir);

        //When we have reached the next tile
        if (transform.position == path[1].center)
        {
            path.RemoveAt(0);
            moving--;

            //If we are done moving this turn
            if (moving == 0)
            {
                audioMovement.Stop();
                SetTile(path[0]);

                //If we've reached our target
                if (path.Count == 1 && orders == UnitOrder.Move)
                {
                    path.Clear();
                    orders = UnitOrder.None;
                }
                busy = false;
            }
        }
    }
    private void AnimateAttack()
    {
        //Play attack animation
        busy = true;

        //If we've just started
        if (attackTimer == 0)
        {
            //Turn to face the target
            Vector3 dir = path[1].center - transform.position;
            transform.rotation = Quaternion.LookRotation(dir);

            //Play sound
            audioAttack.Play();
        }

        //Increse the timer
        attackTimer += Time.deltaTime;

        //When we're done
        if (attackTimer >= attackDuration)
        {
            attackTimer = 0;
            audioAttack.Stop();

            //If we've destroyed the target
            if (combatTarget.Damage(combatPower))
            {
                //Move into the tile
                actionPointsLeft = 1;
                orders = UnitOrder.Move;
                CalculateMovement();
            }
            else //If not
            {
                //Clear any remaining action points and say we're done with our orders
                busy = false;
                actionPointsLeft = 0;
                orders = UnitOrder.None;
                path.Clear();
            }

            //Update UI
            UpdateUI();
        }
    }

    public void Move(HexTile target)
    {
        //If we're currently doing something else or this unit can't attack
        if (!acceptInput() || !canAttack)
            return;

        //Set target
        targetTile = target;
        orders = UnitOrder.Move;

        //Clear any accumulated actionpoints
        accumulatedActionPoints = 0; 

        //Generate path
        path = PathFinding.GetPath(this.tile, target, this);
        CalculateMovement();

        //Update UI
        UpdateUI();
    }
    public void Attack(Entity target)
    {
        //If we're currently doing something else
        if (!acceptInput())
            return;

        //Set target
        orders = UnitOrder.Attack;
        combatTarget = target;
        targetTile = combatTarget.tile;

        //Clear any accumulated actionpoints
        accumulatedActionPoints = 0;

        //Generate path
        path = PathFinding.GetPath(this.tile, targetTile, this);
        CalculateMovement();

        //See if we can attack this round or have to move for some turns first
        if(moving > path.Count-2 || moving >= actionPoints || actionPointsLeft == 0)
            orders = UnitOrder.MoveAttack;

        //Update UI
        UpdateUI();
    }

    public override void OnSelect()
    {
        base.OnSelect();

        UIManager.elements.selectedUnitPanel.gameObject.SetActive(true);
    }
    public override void OnDeselect()
    {
        base.OnDeselect();

        UIManager.elements.selectedUnitPanel.gameObject.SetActive(false);
    }

    public override void UpdateUI()
    {
        if (!isSelected)
            return;

        UIManager.elements.selectedUnitDisplayName.color = team == GameManager.team ? Color.black : Color.red;
        UIManager.elements.selectedUnitDisplayName.text = displayName;
        UIManager.elements.selectedUnitHealth.text = string.Format("Health: {0} / {1}", health, startHealth);
        UIManager.elements.selectedUnitCombatPower.text = string.Format("Combat Power: {0}", combatPower);
        UIManager.elements.selectedUnitActionPoints.text = string.Format("Action Points: {0}{2} / {1}", actionPointsLeft, actionPoints, accumulatedActionPoints == 0 ? "" : string.Format(" (+{0})", accumulatedActionPoints));
    }
}